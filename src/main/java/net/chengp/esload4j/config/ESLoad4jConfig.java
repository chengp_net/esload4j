package net.chengp.esload4j.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:22:18 
 *
 */
@ConfigurationProperties(ESLoad4jConfig.PREFIX)
@Data
public class ESLoad4jConfig {
	
	public static final String PREFIX = "net.chengp.esl4j.config";
	
	private List<MysqlSourceConfig> mysqlSourceConfigs;
	
	@Data
	public static class MysqlSourceConfig {
		
		private String id;
		
		private boolean enable;
		
		private int synDataThreadCount;
		
		private String driver;

		private String host;
		
		private int port;
		
		private String username;
		
		private String password;
		
		private List<DBS> dbs;

	}
	
	@Data
	public static class DBS {
		
		private String database;
		
		private String tables;
		
		private String index;
		
		private String alias;

	}

}
