package net.chengp.esload4j.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:22:11 
 *
 */
@Data
@ConfigurationProperties(ESConfig.PREFIX)
public class ESConfig {
	
	public static final String PREFIX = "es";
	
    private String host;

    private Integer port;
    
    private String username;
    
    private String password;
}
