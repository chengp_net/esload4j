package net.chengp.esload4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import net.chengp.esload4j.config.ESConfig;
import net.chengp.esload4j.config.ESLoad4jConfig;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:24:28 
 *
 */
@SpringBootApplication
@EnableConfigurationProperties({ESLoad4jConfig.class, ESConfig.class})
public class EsLoad4jApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(EsLoad4jApplication.class, args);
	}

}
