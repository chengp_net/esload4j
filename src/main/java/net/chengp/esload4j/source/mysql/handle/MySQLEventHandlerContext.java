package net.chengp.esload4j.source.mysql.handle;

import java.util.HashMap;
import java.util.Map;

import com.github.shyiko.mysql.binlog.event.DeleteRowsEventData;
import com.github.shyiko.mysql.binlog.event.EventData;
import com.github.shyiko.mysql.binlog.event.TableMapEventData;
import com.github.shyiko.mysql.binlog.event.UpdateRowsEventData;
import com.github.shyiko.mysql.binlog.event.WriteRowsEventData;

import net.chengp.esload4j.config.ESLoad4jConfig.DBS;
import net.chengp.esload4j.config.ESLoad4jConfig.MysqlSourceConfig;
import net.chengp.esload4j.source.mysql.handle.evnet.DeleteHandle;
import net.chengp.esload4j.source.mysql.handle.evnet.SaveHandle;
import net.chengp.esload4j.source.mysql.handle.evnet.TableMapHandle;
import net.chengp.esload4j.source.mysql.handle.evnet.UpdateHandle;
import net.chengp.esload4j.target.es.ESLoad;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:23:35
 *
 */
public class MySQLEventHandlerContext {

	public static final Map<Long, TableMapEventData> tableCache = new HashMap<Long, TableMapEventData>();

	public static final Map<String, DBS> listenTable = new HashMap<String, DBS>();

	public AbstractHandler getHandleInstance(EventData eventData, ESLoad ESLoad, MysqlSourceConfig mysqlSourceConfig) {
		if (eventData instanceof WriteRowsEventData) {
			WriteRowsEventData wred = (WriteRowsEventData) eventData;
			return new SaveHandle(wred, ESLoad, mysqlSourceConfig);
		} else if (eventData instanceof DeleteRowsEventData) {
			DeleteRowsEventData dred = (DeleteRowsEventData) eventData;
			return new DeleteHandle(dred, ESLoad, mysqlSourceConfig);
		} else if (eventData instanceof UpdateRowsEventData) {
			UpdateRowsEventData ured = (UpdateRowsEventData) eventData;
			return new UpdateHandle(ured, ESLoad, mysqlSourceConfig);
		} else if (eventData instanceof TableMapEventData) {
			TableMapEventData tmed = (TableMapEventData) eventData;
			return new TableMapHandle(tmed, ESLoad, mysqlSourceConfig);
		} else {
			return null;
		}
	}

}
