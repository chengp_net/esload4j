package net.chengp.esload4j.source.mysql.handle.evnet;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.github.shyiko.mysql.binlog.event.WriteRowsEventData;

import lombok.extern.slf4j.Slf4j;
import net.chengp.esload4j.config.ESLoad4jConfig.MysqlSourceConfig;
import net.chengp.esload4j.source.mysql.handle.AbstractHandler;
import net.chengp.esload4j.target.es.ESLoad;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:23:08 
 *
 */
@Slf4j
public class SaveHandle extends AbstractHandler {

	private WriteRowsEventData wred;

	public SaveHandle(WriteRowsEventData wred, ESLoad ESLoad, MysqlSourceConfig mysqlSourceConfig) {
		super(ESLoad, mysqlSourceConfig);
		this.wred = wred;
	}

	@Override
	public void handle() {
		try {
			List<Map<String, Object>> datas = getOperDatas(wred.getTableId(), wred.getRows());
			if(datas==null||datas.size()<=0) {
				log.warn("新增事件(不处理)：" + JSONObject.toJSONString(wred));
				return;
			}
			boolean r = ESLoad.bulkPutIndex(getIndexName(wred.getTableId()), datas);
			if(r) {
				log.info("新增事件(处理成功)：" + JSONObject.toJSONString(wred));
			}else {
				log.error("新增事件(处理失败)：" + JSONObject.toJSONString(wred));
			}
		} catch (Exception e) {
			log.error("新增事件处理失败!", e);
		}
	}

}
