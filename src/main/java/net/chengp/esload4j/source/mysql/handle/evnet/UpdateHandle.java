package net.chengp.esload4j.source.mysql.handle.evnet;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.github.shyiko.mysql.binlog.event.UpdateRowsEventData;

import lombok.extern.slf4j.Slf4j;
import net.chengp.esload4j.config.ESLoad4jConfig.MysqlSourceConfig;
import net.chengp.esload4j.source.mysql.handle.AbstractHandler;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:23:21 
 *
 */
@Slf4j
public class UpdateHandle extends AbstractHandler {
	
	private UpdateRowsEventData ured;

	public UpdateHandle(UpdateRowsEventData ured, net.chengp.esload4j.target.es.ESLoad eSLoad,
			MysqlSourceConfig mysqlSourceConfig) {
		super(eSLoad, mysqlSourceConfig);
		this.ured = ured;
	}

	@Override
	public void handle() {
		try {
			List<Map<String, Object>> datas = getUpdateDatas(ured.getTableId(), ured.getRows());
			if(datas==null||datas.size()!=2) {
				log.warn("修改事件(不处理)：" + JSONObject.toJSONString(ured));
				return;
			}
			boolean r = ESLoad.updateByQuery(getIndexName(ured.getTableId()), datas.get(0), datas.get(1));
			if(r) {
				log.info("修改事件(处理成功)：" + JSONObject.toJSONString(ured));
			}else {
				log.error("修改事件(处理失败)：" + JSONObject.toJSONString(ured));
			}
		} catch (Exception e) {
			log.error("修改事件处理失败!", e);
		}
	}

}
