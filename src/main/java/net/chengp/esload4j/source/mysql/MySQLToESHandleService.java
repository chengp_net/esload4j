package net.chengp.esload4j.source.mysql;

import com.github.shyiko.mysql.binlog.event.EventData;

import net.chengp.esload4j.config.ESLoad4jConfig.MysqlSourceConfig;
import net.chengp.esload4j.source.mysql.handle.AbstractHandler;
import net.chengp.esload4j.source.mysql.handle.MySQLEventHandlerContext;
import net.chengp.esload4j.target.es.ESLoad;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:23:58 
 *
 */
public class MySQLToESHandleService {
	
	private MysqlSourceConfig mysqlSourceConfig;
	
	private ESLoad ESLoad;
	
	private MySQLEventHandlerContext context;
	
	public MySQLToESHandleService(MysqlSourceConfig mysqlSourceConfig, ESLoad ESLoad) {
		this.mysqlSourceConfig = mysqlSourceConfig;
		this.ESLoad = ESLoad;
		this.context = new MySQLEventHandlerContext();
	}
	
	public void handle(EventData eventData) {
		AbstractHandler handler = context.getHandleInstance(eventData, ESLoad, mysqlSourceConfig);
		if(handler!=null) {
			handler.handle();
		}
	}
	
}
