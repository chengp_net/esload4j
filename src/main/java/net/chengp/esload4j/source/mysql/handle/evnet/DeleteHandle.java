package net.chengp.esload4j.source.mysql.handle.evnet;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.github.shyiko.mysql.binlog.event.DeleteRowsEventData;

import lombok.extern.slf4j.Slf4j;
import net.chengp.esload4j.config.ESLoad4jConfig.MysqlSourceConfig;
import net.chengp.esload4j.source.mysql.handle.AbstractHandler;
import net.chengp.esload4j.target.es.ESLoad;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:23:01 
 *
 */
@Slf4j
public class DeleteHandle extends AbstractHandler {
	
	private DeleteRowsEventData dred;

	public DeleteHandle(DeleteRowsEventData dred, ESLoad eSLoad,
			MysqlSourceConfig mysqlSourceConfig) {
		super(eSLoad, mysqlSourceConfig);
		this.dred = dred;
	}

	@Override
	public void handle() {
		try {
			List<Map<String, Object>> datas = getOperDatas(dred.getTableId(), dred.getRows());
			if(datas==null||datas.size()<=0) {
				log.warn("删除事件(不处理)：" + JSONObject.toJSONString(dred));
				return;
			}
			boolean r = ESLoad.deleteByQuery(getIndexName(dred.getTableId()), datas);
			if(r) {
				log.info("删除事件(处理成功)：" + JSONObject.toJSONString(dred));
			}else {
				log.error("删除事件(处理失败)：" + JSONObject.toJSONString(dred));
			}
		} catch (Exception e) {
			log.error("刪除事件处理失败!", e);
		}
	}

}
