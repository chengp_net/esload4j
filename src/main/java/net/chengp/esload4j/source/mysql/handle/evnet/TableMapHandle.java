package net.chengp.esload4j.source.mysql.handle.evnet;

import com.github.shyiko.mysql.binlog.event.TableMapEventData;

import lombok.extern.slf4j.Slf4j;
import net.chengp.esload4j.config.ESLoad4jConfig.MysqlSourceConfig;
import net.chengp.esload4j.source.mysql.handle.AbstractHandler;
import net.chengp.esload4j.source.mysql.handle.MySQLEventHandlerContext;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:23:15 
 *
 */
@Slf4j
public class TableMapHandle extends AbstractHandler {
	
	private TableMapEventData tmed;

	public TableMapHandle(TableMapEventData tmed, net.chengp.esload4j.target.es.ESLoad eSLoad,
			MysqlSourceConfig mysqlSourceConfig) {
		super(eSLoad, mysqlSourceConfig);
		this.tmed = tmed;
	}

	@Override
	public void handle() {
		try {
			if (MySQLEventHandlerContext.listenTable.containsKey(tmed.getDatabase() +"."+ tmed.getTable())) {
				MySQLEventHandlerContext.tableCache.put(tmed.getTableId(), tmed);
			}
		} catch (Exception e) {
			log.error("TableMap事件处理失败!", e);
		}
	}

}
