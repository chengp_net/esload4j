package net.chengp.esload4j.source.mysql;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;
import net.chengp.esload4j.config.ESLoad4jConfig;
import net.chengp.esload4j.config.ESLoad4jConfig.MysqlSourceConfig;
import net.chengp.esload4j.target.es.ESLoad;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:23:43 
 *
 */
@Slf4j
@Component
public class MySQLToES {

	@Autowired
	private ESLoad4jConfig ESLoad4jConfig;

	@Autowired
	private ESLoad ESLoad;

	@PostConstruct
	public void startSyn() {
		try {
			log.info("加载MySQL数据源配置：" + JSONObject.toJSONString(ESLoad4jConfig));
			if (ESLoad4jConfig != null && ESLoad4jConfig.getMysqlSourceConfigs() != null
					&& ESLoad4jConfig.getMysqlSourceConfigs().size() > 0) {
				for (MysqlSourceConfig mysqlSourceConfig : ESLoad4jConfig.getMysqlSourceConfigs()) {
					if (!mysqlSourceConfig.isEnable()) {
						continue;
					}
					new Thread(new MySQLToESHandleThread(mysqlSourceConfig, ESLoad)).start();
				}
			}
		} catch (Exception e) {
			log.error("同步MySQL数据至ES失败！");

		}
	}

}
