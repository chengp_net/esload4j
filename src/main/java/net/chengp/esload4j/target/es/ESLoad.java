package net.chengp.esload4j.target.es;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:24:14 
 *
 */
public interface ESLoad {
	
	boolean indexExists(String index)throws Exception;
	
	boolean createIndex(String index, String alias)throws Exception;

	boolean bulkPutIndex(String index, List<Map<String, Object>> list) throws Exception;
	
	boolean bulkPutIndex(String index, Map<String, Object> list) throws Exception;
	
	boolean deleteByQuery(String index, List<Map<String, Object>> query)throws Exception;
	
	boolean deleteByQuery(String index, Map<String, Object> query)throws Exception;
	
	boolean updateByQuery(String index, Map<String, Object> query, Map<String, Object> data)throws Exception;

}
