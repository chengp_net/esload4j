package net.chengp.esload4j.target.es;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.elasticsearch.action.admin.indices.alias.Alias;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:24:21 
 *
 */
@Component
@Slf4j
public class ESLoadImpl implements ESLoad {
	
	@Autowired
	private RestHighLevelClient restHighLevelClient;
	
	@Override
	public boolean indexExists(String index) throws Exception {
		GetIndexRequest request = new GetIndexRequest(index);
		return restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
	}
	
	@Override
	public boolean createIndex(String index, String alias) throws Exception {
		GetIndexRequest request = new GetIndexRequest(index);
		boolean exists = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
		if (!exists) {
			CreateIndexRequest createRequest = new CreateIndexRequest(index);
			if (!StringUtils.isEmpty(alias)) {
				createRequest.alias(new Alias(alias));
			}
			CreateIndexResponse created = restHighLevelClient.indices().create(createRequest,
					RequestOptions.DEFAULT);
			if (!created.isAcknowledged()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean bulkPutIndex(String index, List<Map<String, Object>> list) throws IOException {
		if(StringUtils.isEmpty(index) || list == null || list.size() <= 0) {
			return false;
		}
		BulkRequest request = new BulkRequest();
		list.forEach(e -> {
			request.add(new IndexRequest(index).opType("create").id(UUID.randomUUID().toString()).source(e));
		});
		BulkResponse response = restHighLevelClient.bulk(request, RequestOptions.DEFAULT);
		if(response.hasFailures()) {
			log.error(response.buildFailureMessage());
		}
		return !response.hasFailures();
	}
	
	@Override
	public boolean bulkPutIndex(String index, Map<String, Object> data) throws IOException {
		List<Map<String, Object>> datas = new ArrayList<Map<String, Object>>();
		datas.add(data);
		return bulkPutIndex(index, datas);
	}

	@Override
	public boolean deleteByQuery(String index, List<Map<String, Object>> datas) throws Exception {
		if(StringUtils.isEmpty(index) || datas == null || datas.size() <= 0) {
			return false;
		}
		DeleteByQueryRequest request = new DeleteByQueryRequest(index);
		for(Map<String, Object> query:datas) {
			query.entrySet().forEach(e->{
				request.setQuery(new TermQueryBuilder(e.getKey(), e.getValue()==null?"":e.getValue()));
				request.setMaxDocs(1);
			});
			restHighLevelClient.deleteByQuery(request, RequestOptions.DEFAULT);
		}
		return true;
	}
	
	@Override
	public boolean deleteByQuery(String index, Map<String, Object> query) throws Exception {
		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		data.add(query);
		return deleteByQuery(index, data);
	}

	@Override
	public boolean updateByQuery(String index, Map<String, Object> query, Map<String, Object> data) throws Exception {
		boolean ir = bulkPutIndex(index, data);
		if(ir) {
			return deleteByQuery(index, query);
		}
		return false;
	}

}
