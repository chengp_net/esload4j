package net.chengp.esload4j.db.kv;

import java.util.concurrent.ConcurrentMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:22:47 
 *
 */
public class MapDBUtils {

	private static final String filePath = "esl4j.db";

	private static final String ESL4J = "esl4j";

	private static DB db = null;

	private static void init() {
		if (db == null || db.isClosed()) {
			db = DBMaker.fileDB(filePath).closeOnJvmShutdown().make();
		}
	}

	public static void put(String key, String value) {
		init();
		ConcurrentMap<String, String> map = db.hashMap(ESL4J).keySerializer(Serializer.STRING)
				.valueSerializer(Serializer.STRING).createOrOpen();
		map.put(key, value);
		db.close();
	}

	public static String get(String key) {
		init();
		ConcurrentMap<String, String> map = db.hashMap(ESL4J).keySerializer(Serializer.STRING)
				.valueSerializer(Serializer.STRING).createOrOpen();
		String value = map.get(key);
		db.close();
		return value;
	}

}
