package net.chengp.esload4j.db.jdbc;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author chengp
 * @email E-mail:chengp@chengp.net
 * @version 创建时间：2020年9月12日 下午3:22:24 
 *
 */
public interface BaseDao {

	<T> T query(String sql, Object[] params, Class<T> clazz)throws Exception;

	<T> List<T> queryAll(String sql, Object[] params, Class<T> clazz)throws Exception;
	
	List<Map<String, Object>> queryAllForMap(String sql, Object[] params)throws Exception;
	
	long count(String tableName)throws Exception;
	
}