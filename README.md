# ESLoad4j

#### 介绍
ESLoad4j是一个将MySQL数据同步至ES(ElasticSearch)的小工具，采用Java SpringBoot开发，一行java -jar命令即可启动，支持全量或者增量将MySQL数据同步至ES，支持断点续同。

#### 软件架构
![ESLoad4j软件架构](https://images.gitee.com/uploads/images/2020/0912/162902_181520fd_4935065.png "屏幕截图.png")


#### 运行教程

*需要有jdk1.8环境
1.  下载ESLoad4j [下载链接](https://gitee.com/chengp_net/esload4j/releases)；
2.  解压ESLoad4j-x.x.zip文件，里面会有一个jar和一个application.yml配置文件，我们需要修改配置文件，主要配置mysql连接信息、es连接信息以及需要同步的数据库表，具体可参考配置文件注释说明
3.  执行java命令`java -jar ESLoad4j-1.0.jar`；
![输入图片说明](https://images.gitee.com/uploads/images/2021/0825/165023_db701f55_4935065.png "屏幕截图.png")
*启动后目录会生成一个logs目录和一个db文件，logs主要记录同步日志，db文件主要记录binlog位置，方便停机后从上次位置开始同步。


#### 使用说明

1.  软件运行前需要确保有java 1.8x环境以及mysql开启了binlog日志且日志格式为ROW；
2.  软件运行时会加载application.yml配置文件，主要加载mysql连接及需要同步的数据库表以及目标ES连接配置，程序启动后首先会根据配置判断ES索引是否存在，若不存在则程序自动创建ES索引且全量加载数据至ES索引，若存在ES索引则根据mysql-binlog日志实时同步数据至数据库表对应的索引，支持增、删、改，其中改通过删除&新增间接实现；
3.  软件中途停止重新启动无需特殊操作，程序会在停止前自动记录好binlog文件位置到.db文件，下次启动会从原来的位置开始读取加载数据。


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
